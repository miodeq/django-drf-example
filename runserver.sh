#!/bin/sh
clear;

find . -name "*.pyc" | xargs rm && \

rm -rf *.sqlite3 && \

python manage.py makemigrations && \
python manage.py migrate && \

USER="admin"
EMAIL="admin@admin.pl"
PASSWORD="admin"

echo "${0}: adding initial data..."
python manage.py create_people

echo "from django.contrib.auth.models import User; print(\"Admin exists\") if User.objects.filter(username='admin').exists() else User.objects.create_superuser('$USER', '$EMAIL', '$PASSWORD')" | python manage.py shell && \

python manage.py runserver