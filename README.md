### Generate TOKEN:
```bash
curl -i -X POST http://localhost:8000/api/api-token-auth/ -H 'Content-Type: application/json' -d '{"username": "admin", "password": "admin"}'
```

### Get list of objects:
```bash
curl http://localhost:8000/api/people/ -H 'Authorization: Token 318fc08cacfd7e3daaf9bd55089afbb1e3162264'
```

### Get specific person object:
```bash
curl http://localhost:8000/api/person/1/ -H 'Authorization: Token 318fc08cacfd7e3daaf9bd55089afbb1e3162264' -H 'Content-Type: application/json'
```

### Running celery task through admin:
#### admin.py
```python
from django.contrib import messages


def do_something(modeladmin, request, queryset):
    """
    Admin action to call a celery task.
    """

    from my_app.tasks import my_task_name

    items_list = list(queryset.values_list('pk', flat=True))

    my_task_name.delay(
        items_list,
        queryset.model._meta.app_label,
        queryset.model.__name__
    )

    modeladmin.message_user(
        request,
        f'Successfully run task for {queryset.count()} objects.',
        level=messages.SUCCESS
    )

do_something.short_description = 'Run celery task'

...

admin.site.add_action(do_something)
```

#### tasks.py
```python
@shared_task(
    name='my_task_name',
    queue='my_task_queue',
    ignore_result=True
)
def my_task_name(objects_data, app_label, model_name):
    """
    Celery task to do something for given data in queryset.
    """

    from django.apps import apps

    model = apps.get_model(app_label, model_name)

    files_data = model.objects.filter(pk__in=objects_data)
    
    for file_item in files_data:
        print(file_item)
```