# -*- coding: utf-8 -*-

from django.core.management import BaseCommand

from api.models import People


class Command(BaseCommand):
    """
    External manage.py command

    Example of usage:
        ./manage.py create_people
    """

    help = 'Creates initial data for models.'

    def handle(self, *args, **options):
        """
        Override BaseCommand handle method.
        """

        names = ('Janek', 'Franek', 'Andy', 'Zbyszek')
        surnames = ('Dzbanek', 'Szybki', 'Woodpecker', 'Trzy cytryny')

        data = list(zip(names, surnames))

        People.objects.bulk_create([
            People(
                name=x[0],
                surname=x[1]
            ) for x in data
        ])
