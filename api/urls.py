# -*- coding: utf-8 -*-

from django.urls import path, re_path
from rest_framework.authtoken.views import obtain_auth_token

from api.views import PeopleListView, PersonView

urlpatterns = (
    path("api-token-auth/", obtain_auth_token, name="api_token_auth"),
    path("people/", PeopleListView.as_view(), name="people"),
    re_path("person/(?P<pk>\d+)/$", PersonView.as_view(), name="person"),
)
