from django.db import models


class People(models.Model):
    """
    People model.
    """

    name = models.CharField(
        verbose_name='Name',
        blank=True,
        null=True,
        max_length=255
    )

    surname = models.CharField(
        verbose_name='Surname',
        blank=True,
        null=True,
        max_length=255
    )

    dt = models.DateTimeField(
        verbose_name='Date',
        editable=False,
        auto_now=True
    )

    class Meta:
        verbose_name = 'People'
        verbose_name_plural = 'People'

    def __str__(self):
        return self.name
