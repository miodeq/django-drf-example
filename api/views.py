# -*- coding: utf-8 -*-

from django.http import Http404
from rest_framework import status, authentication
from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response

from api.models import People
from api.serializers import PeopleSerializer


# TODO:
# Create APIView (POST) to create a new People model object.
# Update README with an example of curl request

class PeopleListView(ListAPIView):
    authentication_classes = (authentication.TokenAuthentication,)

    renderer_classes = (JSONRenderer,)
    http_method_names = ("get",)

    serializer_class = PeopleSerializer
    queryset = People.objects.all()

    def list(self, request, *args, **kwargs):
        """
        Returns all persons names.
        """

        serializer = self.get_serializer(
            self.get_queryset(),
            many=True,
            read_only=True
        )

        return Response(
            data=serializer.data,
            status=status.HTTP_200_OK
        )


class PersonView(RetrieveAPIView):
    serializer_class = PeopleSerializer
    queryset = People.objects.all()

    authentication_classes = (authentication.TokenAuthentication,)

    renderer_classes = (JSONRenderer,)
    http_method_names = ("get",)

    def retrieve(self, request, *args, **kwargs):
        """
        Returns data for specific person.
        """

        try:
            serializer = self.get_serializer(
                self.get_object(), read_only=True)

            return Response(
                data=serializer.data,
                status=status.HTTP_200_OK
            )
        except Http404:
            return Response(
                data={},
                status=status.HTTP_404_NOT_FOUND
            )
