# -*- coding: utf-8 -*-

from rest_framework import serializers

from api.models import People


class PeopleSerializer(serializers.ModelSerializer):
    """
    PeopleSerializer for People model.
    """

    class Meta:
        model = People

        fields = '__all__'
